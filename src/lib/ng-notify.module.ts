import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifyComponent } from './no-notify/notify/notify.component';
import { NotifyListComponent } from './no-notify/notify-list/notify-list.component';
import {NotifyService} from './no-notify/notify.service';
import {NotifyConfigService} from './no-notify/notify-config.service';
import { TempComponent } from './no-notify/temp/temp.component';
import { TempcComponent } from './no-notify/tempc/tempc.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        NotifyListComponent,
        NotifyComponent
    ],
    exports: [
        NotifyListComponent,
        NotifyComponent
    ]
})
export class NgNotifyModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: NgNotifyModule,
            providers: [NotifyService, NotifyConfigService],
        };
    }
}

