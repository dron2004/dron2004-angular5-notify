import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {Notify} from './notify';
import {NotifyConfigService} from './notify-config.service';
import {Subscription} from 'rxjs/Subscription';
import {NotifyWindowPosition} from './notify.constants';

@Injectable()
export class NotifyService implements OnInit, OnDestroy {
    private _notifies: Array<Notify> = [];

    constructor(
        private config: NotifyConfigService
    ) {
    }

    ngOnInit() {}

    ngOnDestroy() {}

    get notifies(): Array<Notify> {
        return this._notifies;
    }

    add(notify: Notify): void {
        this._notifies.push(notify);
    }

    remove(notify: Notify): void {
        const index: number = this._notifies.indexOf(notify);
        if (index !== -1) {
            this._notifies.splice(index, 1);
        }
    }

    clear(): void {
        this._notifies = [];
    }

    getConfig() {
        return this.config;
    }

    setConfig(options: any) {
        this.config = Object.assign(this.config, options);
    }
}
