import { Component, OnInit } from '@angular/core';
import {NotifyService} from '../notify.service';
import {Notify} from '../notify';
import {NotifyType} from '../notify.constants';

@Component({
  selector: 'app-temp',
  templateUrl: './temp.component.html',
  styleUrls: ['./temp.component.css']
})
export class TempComponent implements OnInit {

    items: Array<Notify> = [];
    constructor(
        private service: NotifyService
    ) {
    }

  ngOnInit() {
      this.items.push(new Notify('Notify Component', NotifyType.DANGER, 20));
      this.items.push(new Notify('for Angular 5+', NotifyType.SUCCESS, 17));
      this.items.push(new Notify('no jQuery use', NotifyType.LIGHT, 55));
  }
}
