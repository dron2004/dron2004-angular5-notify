import {Component, Input, OnInit} from '@angular/core';
import {Notify} from '../notify';

@Component({
  selector: 'app-tempc',
  templateUrl: './tempc.component.html',
  styleUrls: ['./tempc.component.css']
})
export class TempcComponent implements OnInit {
    @Input() notify: Notify;
    constructor() { }

    ngOnInit() {
    }

}
