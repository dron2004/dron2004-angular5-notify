import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Notify} from '../notify';
import {NotifyType, NotifyWindowPosition} from '../notify.constants';
import {NotifyService} from '../notify.service';
import {NotifyConfigService} from '../notify-config.service';

@Component({
    selector: 'app-notify-list',
    templateUrl: './notify-list.component.html',
    styleUrls: ['./notify-list.component.css'],
})
export class NotifyListComponent implements OnInit {
    @Input() notifies: Array<Notify> = [];
    @Input() options: any;

    private _window: any;

    @HostListener('window:resize', ['$event']) onResize(event) {
        this._window = {width: event.target.innerWidth, height: event.target.innerHeight};
    }

    constructor(
        private service: NotifyService
    ) {
    }

    get notifyList(): Array<Notify> {
        return this.service.notifies;
    }

    get listPosition() {
        return this.service.getConfig().notifiesPosition;
    }

    ngOnInit() {
        this.service.setConfig(this.options);
        this._window = {width: window.innerWidth, height: window.innerHeight};
    }

    remove (notify: Notify) {
        this.service.remove(notify);
    }

    // get listPos(): Object {
    //     let top, bottom, left, right: number | string;
    //     const middleY = Math.round(((this._window.height / 2 - 100) / this._window.height) * 100) + '%';
    //     const middleX = Math.round(((this._window.width / 2 - 100) / this._window.width) * 100) + '%';
    //     switch (this.listPosition) {
    //         case NotifyWindowPosition.TOPLEFT:
    //             top = 0;
    //             left = 0;
    //             break;
    //         case NotifyWindowPosition.TOPRIGHT:
    //             top = 0;
    //             right = 0;
    //             break;
    //         case NotifyWindowPosition.TOPCENTER:
    //             left = middleX;
    //             top = 0;
    //             break;
    //         case NotifyWindowPosition.BOTTOMLEFT:
    //             bottom = 0;
    //             left = 0;
    //             break;
    //         case NotifyWindowPosition.BOTTOMRIGHT:
    //             bottom = 0;
    //             right = 0;
    //             break;
    //         case NotifyWindowPosition.BOTTOMCENTER:
    //             left = middleX;
    //             bottom = 0;
    //             break;
    //         case NotifyWindowPosition.CENTERLEFT:
    //             top = middleY;
    //             left = 0;
    //             break;
    //         case NotifyWindowPosition.CENTERRIGHT:
    //             top = middleY;
    //             right = 0;
    //             break;
    //         case NotifyWindowPosition.CENTER:
    //         default:
    //             top = middleY;
    //             left = middleX;
    //             break;
    //     }
    //     const res =  {}; // {top: top, right: right, bottom: bottom, left: left};
    //     // console.log(res);
    //     return res;
    // }

    get listPosClass(): string {
        let res = 'notify-list ';
        switch (this.listPosition) {
            case NotifyWindowPosition.TOPLEFT:
                res += 'top left';
                break;
            case NotifyWindowPosition.TOPRIGHT:
                res += 'top right';
                break;
            case NotifyWindowPosition.TOPCENTER:
                res += 'top centerX';
                break;
            case NotifyWindowPosition.BOTTOMLEFT:
                res += 'bottom left';
                break;
            case NotifyWindowPosition.BOTTOMRIGHT:
                res += 'bottom right';
                break;
            case NotifyWindowPosition.BOTTOMCENTER:
                res += 'bottom centerX';
                break;
            case NotifyWindowPosition.CENTERLEFT:
                res += 'centerY left';
                break;
            case NotifyWindowPosition.CENTERRIGHT:
                res += 'centerY right';
                break;
            case NotifyWindowPosition.CENTER:
            default:
                res += 'centerY centerX';
                break;
        }
        return res;
    }
}
