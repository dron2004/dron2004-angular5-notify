export enum NotifyWindowPosition {
    TOPLEFT = 11,
    TOPRIGHT = 12,
    TOPCENTER = 13,
    BOTTOMLEFT = 21,
    BOTTOMRIGHT = 22,
    BOTTOMCENTER = 23,
    CENTER = 30,
    CENTERLEFT = 31,
    CENTERRIGHT = 32,
}

export enum NotifyLinePosition {
    START = 1,
    END = 2,
    MIDDLE = 3,
}

export enum NotifyType {
    PRIMARY = 1,
    SECONDARY = 2,
    SUCCESS = 3,
    DANGER = 4,
    WARNING = 5,
    INFO = 6,
    LIGHT = 7,
    DARK = 8,
}
