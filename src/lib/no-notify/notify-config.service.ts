import { Injectable } from '@angular/core';
import {NotifyLinePosition, NotifyWindowPosition} from './notify.constants';

@Injectable()
export class NotifyConfigService {
    notifiesPosition = NotifyWindowPosition.BOTTOMCENTER;
    defaultTimeout = 10;
}
