import {NotifyLinePosition, NotifyType, NotifyWindowPosition} from './notify.constants';

export class Notify {
    private _type: NotifyType;
    private _text: string;
    private _timeout: number;

    constructor(text: string, type?: NotifyType, timeout?: number) {
        this._text = text;
        this._type = type;
        this._timeout = timeout;
    }

    get timeout(): number {
        return this._timeout;
    }

    set timeout(timeout: number) {
        this._timeout = timeout;
    }

    get text(): string {
        return this._text;
    }

    get type(): NotifyType {
        return this._type;
    }
}
