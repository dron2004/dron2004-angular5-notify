import { TestBed, inject } from '@angular/core/testing';

import { NotifyConfigService } from './notify-config.service';

describe('NotifyConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotifyConfigService]
    });
  });

  it('should be created', inject([NotifyConfigService], (service: NotifyConfigService) => {
    expect(service).toBeTruthy();
  }));
});
