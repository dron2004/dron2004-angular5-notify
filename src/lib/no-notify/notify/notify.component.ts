import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NotifyType, NotifyWindowPosition} from '../notify.constants';
import {Notify} from '../notify';
import {NotifyConfigService} from '../notify-config.service';

function getShownAnimation() {
    return {opacity: 1};
}

@Component({
    selector: 'app-notify',
    templateUrl: './notify.component.html',
    styleUrls: ['./notify.component.css'],
})
export class NotifyComponent implements OnInit, AfterViewInit {
    @Input() notify: Notify;
    @Input() listPosition: NotifyWindowPosition;
    @Output() closeNotify: EventEmitter<Notify> = new EventEmitter();
    show: boolean = false;

    constructor(private config: NotifyConfigService) {}

    getAnimationClass(): string {
        switch (this.listPosition) {
            case NotifyWindowPosition.TOPLEFT:
                return 'fromLeft';
            case NotifyWindowPosition.TOPRIGHT:
                return 'fromRight';
            case NotifyWindowPosition.TOPCENTER:
                return 'fromTop';
            case NotifyWindowPosition.BOTTOMLEFT:
                return 'fromLeft';
            case NotifyWindowPosition.BOTTOMRIGHT:
                return 'fromRight';
            case NotifyWindowPosition.BOTTOMCENTER:
                return 'fromDown';
            case NotifyWindowPosition.CENTERLEFT:
                return 'fromLeft';
            case NotifyWindowPosition.CENTERRIGHT:
                return 'fromRight';
            case NotifyWindowPosition.CENTER:
            default:
                return 'fade';
        }
    }

    getTextPositionClass(): string {
        switch (this.listPosition) {
            case NotifyWindowPosition.TOPLEFT:
            case NotifyWindowPosition.TOPRIGHT:
            case NotifyWindowPosition.BOTTOMLEFT:
            case NotifyWindowPosition.BOTTOMRIGHT:
            case NotifyWindowPosition.CENTERLEFT:
            case NotifyWindowPosition.CENTERRIGHT:
                return 'text-left';
            case NotifyWindowPosition.TOPCENTER:
            case NotifyWindowPosition.BOTTOMCENTER:
            case NotifyWindowPosition.CENTER:
            default:
                return 'text-center';
        }
    }

    get text(): string {
        return this.notify.text;
    }

    get type(): NotifyType {
        return this.notify.type;
    }

    ngOnInit() {
    }

    ngAfterViewInit(): void {
        // задержка для анимации состояния
        setTimeout(() => {
            this.show = true;
        }, 200);
        // автозакрытие по таймауту
        if (typeof this.notify.timeout === 'undefined') {
            this.notify.timeout = this.config.defaultTimeout;
        }
        setTimeout(() => {
            this.close();
        }, this.notify.timeout * 1000);
    }

    onClick (event: MouseEvent): void {
        this.close();
    }

    close() {
        this.show = false;
        // задержка для анимации состояния
        setTimeout(() => { this.closeNotify.emit(this.notify); }, 200 );
    }



    get notifyType(): string {
        switch (this.type) {
            case NotifyType.PRIMARY:
                return 'primary';
            case NotifyType.DANGER:
                return 'danger';
            case NotifyType.WARNING:
                return 'warning';
            case NotifyType.SECONDARY:
                return 'secondary';
            case NotifyType.DARK:
                return 'dark';
            case NotifyType.INFO:
                return 'info';
            case NotifyType.LIGHT:
                return 'light';
            case NotifyType.SUCCESS:
                return 'success';
            default:
                return 'default';
        }
    }


    getTypeClassName() {
        return 'bg-' + this.notifyType;
    }
}
