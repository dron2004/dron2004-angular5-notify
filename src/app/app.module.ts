import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {NgNotifyModule} from '../lib/ng-notify.module';



@NgModule({
  declarations: [
      AppComponent,
  ],
  imports: [
      BrowserModule,
      NgNotifyModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
