import {Component, OnInit} from '@angular/core';
import {NotifyService} from '../lib/no-notify/notify.service';
import {Notify} from '../lib/no-notify/notify';
import {NotifyType, NotifyWindowPosition} from '../lib/no-notify/notify.constants';
import {NotifyConfigService} from '../lib/no-notify/notify-config.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
    NotifyWindowPosition = NotifyWindowPosition;
    title = 'Notify Test App';
    lastPosition: NotifyWindowPosition;

    constructor (
        private notifier: NotifyService,
        ) {
    }

    ngOnInit(): void {
        setTimeout(() => {
            this.notifier.add(new Notify('Notify Component', NotifyType.DANGER, 20));
        }, 200);
        setTimeout(() => {
            this.notifier.add(new Notify('for Angular 5+', NotifyType.SUCCESS, 17));
        }, 300);
        setTimeout(() => {
            this.notifier.add(new Notify('no jQuery use', NotifyType.LIGHT, 55));
        }, 400);



    }

    testNotify(position: NotifyWindowPosition): void {
        if (this.lastPosition === position) {
            this.notifier.add(new Notify('I`m a new Notify window', this.getRandomType(), 15));
        } else {
            this.notifier.clear();
            this.notifier.setConfig({notifiesPosition: position});
            setTimeout(() => {
                this.notifier.add(new Notify('This is simple Notify with danger class', NotifyType.DANGER, 15));
            }, 200);
            setTimeout(() => {
                this.notifier.add(new Notify('Short notify text', NotifyType.SUCCESS, 5));
            }, 1000);
            setTimeout(() => {
                this.notifier.add(new Notify('Notify dark class', NotifyType.DARK, 6));
            }, 1500);
            this.lastPosition = position;
        }
    }

    getRandomType() {
        const enumValues = Object.keys(NotifyType)
            .map(n => Number.parseInt(n))
            .filter(n => !Number.isNaN(n));
        const randomIndex = this.getRandomInt(0, enumValues.length);
        return enumValues[randomIndex];
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; // The maximum is exclusive and the minimum is inclusive
    }
}
